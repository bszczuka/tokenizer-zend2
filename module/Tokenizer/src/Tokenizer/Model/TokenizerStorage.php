<?php

namespace Tokenizer\Model;

use Zend\Authentication\Storage;

class TokenizerStorage extends Storage\Session 
{

    public function setEmail($email = NULL) 
    {
        if ($email) 
        {
            $this->session->user_email = $email;
        }
    }

    public function getEmail() 
    {
        return $this->session->user_email;
    }

    public function clearEmail() 
    {
        $this->session->user_email = NULL;
    }

    public function setPassword($password = NULL) 
    {
        if ($password)
        {
            $this->session->user_password = $password;
        }
    }

    public function getPassword() 
    {
        return $this->session->user_password;
    }

    public function clearPassword()
    {
        $this->session->user_password = NULL;
    }

}
