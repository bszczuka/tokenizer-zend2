<?php

namespace Tokenizer\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;
use Tokenizer\Model\User;

define('APP_ID', '33');
define('APP_KEY', '10454895428c3d17011cfd555043bfbe');


define('APP_URL', 'http://zend2.dev/auth/verify?id=');

class AuthController extends AbstractActionController 
{
    protected $form;
    protected $storage;
    protected $authservice;

    public function getAuthService() 
    {
        if (!$this->authservice) 
        {
            $this->authservice = $this->getServiceLocator()
                    ->get('AuthService');
        }

        return $this->authservice;
    }

    public function getSessionStorage()
    {
        if (!$this->storage) 
        {
            $this->storage = $this->getServiceLocator()
                    ->get('Tokenizer\Model\TokenizerStorage');
        }

        return $this->storage;
    }

    public function getForm()
    {
        if (!$this->form) 
        {
            $user = new \Tokenizer\Model\User();
            $builder = new AnnotationBuilder();
            $this->form = $builder->createForm($user);
        }

        return $this->form;
    }

    public function dbAuthenticate($email, $password)
    {
        $this->getAuthService()->getAdapter()
                ->setIdentity($email)
                ->setCredential($password);

        return $this->getAuthService()->authenticate();
    }

    public function tokenize()
    {
        $email = $this->getSessionStorage()->getEmail();

        $this->getAuthService()->clearIdentity();

        try 
        {
            $tokenizer = new \AmsterdamStandard\Tokenizer([
                'app_id' => APP_ID,
                'app_key' => APP_KEY,
            ]);

            $tokenizer->createAuth($email, APP_URL, $redirect = true);
        } 
        catch (\AmsterdamStandard\Tokenizer\Exception $e) 
        {
            echo $e->getMessage();
            exit;
        }
    }

    public function loginAction() 
    {
        if ($this->getAuthService()->hasIdentity()) 
        {
            return $this->redirect()->toRoute('success');
        }

        $form = $this->getForm();

        return array(
            'form' => $form
        );
    }

    public function verifyAction() 
    {
        try 
        {
            $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
            if (!$id) 
            {
                throw new \AmsterdamStandard\Tokenizer\Exception("No url id found");
            }

            $tokenizer = new \AmsterdamStandard\Tokenizer([
                'app_id' => APP_ID,
                'app_key' => APP_KEY,
            ]);

            if ($tokenizer->getSession('tokenizer_id') === null) 
            {
                throw new \AmsterdamStandard\Tokenizer\Exception("No tokenizer session found");
            }

            if ($tokenizer->getSession('tokenizer_id') != $id) 
            {
                throw new \AmsterdamStandard\Tokenizer\Exception("Tokenizer session does not match url id");
            }

            if ($tokenizer->verifyAuth($id))
            {
                $this->dbAuthenticate($this->getSessionStorage()->getEmail(), $this->getSessionStorage()->getPassword());
                $this->getSessionStorage()->clearEmail();
                $this->getSessionStorage()->clearPassword();
                $this->flashmessenger()->addMessage("You are logged in");
                $this->redirect()->toRoute('auth');
            } 
            else 
            {
                $this->flashmessenger()->addMessage("Authentication has been rejected");
                $this->redirect()->toRoute('auth');
            }
        }
        catch (\AmsterdamStandard\Tokenizer\Exception $e) 
        {
            echo $e->getMessage();
            exit;
        }
    }

    public function authenticateAction() 
    {
        $form = $this->getForm();
        $redirect = 'auth';

        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $form->setData($request->getPost());
            if ($form->isValid()) 
            {
                $result = $this->dbAuthenticate($request->getPost('email'), $request->getPost('password'));

                if ($result->isValid()) 
                {
                    $redirect = 'auth/tokenize';
                    $this->getSessionStorage()->setEmail($request->getPost('email'));
                    $this->getSessionStorage()->setPassword($request->getPost('password'));
                    $this->tokenize();
                }
            }
        }

        return $this->redirect()->toRoute('auth');
    }

    public function logoutAction() 
    {
        $this->getSessionStorage()->clearEmail();
        $this->getAuthService()->clearIdentity();

        $this->flashmessenger()->addMessage("You've been logged out");
        return $this->redirect()->toRoute('auth');
    }

}
