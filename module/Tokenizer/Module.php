<?php
namespace Tokenizer;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Session\Container;

class Module 
{
    public function onBootstrap(MvcEvent $e) 
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $this->bootstrapSession($e);
    }
    public function bootstrapSession($e)
    {
        $session = $e->getApplication()
                     ->getServiceManager()
                     ->get('Zend\Session\SessionManager');
        $session->start();


        $container = new Container('initialized');
        if (!isset($container->init)) 
        {
             $session->regenerateId(true);
             $container->init = 1;
        }
    }
    

    public function getConfig() 
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() 
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() 
    {
        return array(
            'factories' => array(
                'Zend\Session\SessionManager' => function ($sm) 
                {
                    $config = $sm->get('config');
                    if (isset($config['session'])) 
                    {
                        $session = $config['session'];
                        $sessionConfig = null;
                        if (isset($session['config'])) 
                        {
                            $class = isset($session['config']['class'])  ? $session['config']['class'] : 'Zend\Session\Config\SessionConfig';
                            $options = isset($session['config']['options']) ? $session['config']['options'] : array();
                            $sessionConfig = new $class();
                            $sessionConfig->setOptions($options);
                        }

                        $sessionStorage = null;
                        if (isset($session['storage'])) 
                        {
                            $class = $session['storage'];
                            $sessionStorage = new $class();
                        }

                        $sessionSaveHandler = null;
                        if (isset($session['save_handler'])) 
                        {
                            // class should be fetched from service manager since it will require constructor arguments
                            $sessionSaveHandler = $sm->get($session['save_handler']);
                        }

                        $sessionManager = new SessionManager($sessionConfig, $sessionStorage, $sessionSaveHandler);

                        if (isset($session['validator'])) 
                        {
                            $chain = $sessionManager->getValidatorChain();
                            foreach ($session['validator'] as $validator) 
                            {
                                $validator = new $validator();
                                $chain->attach('session.validate', array($validator, 'isValid'));

                            }
                        }
                    } 
                    else 
                    {
                        $sessionManager = new SessionManager();
                    }
                    Container::setDefaultManager($sessionManager);
                    return $sessionManager;
                },
                'Tokenizer\Model\TokenizerStorage' => function($sm) 
                {
                    return new \Tokenizer\Model\TokenizerStorage('tokenizer');
                },
                'AuthService' => function($sm) 
                {
                    $config = array(
                        'driver' => 'Mysqli',
                        'database' => 'zend2',
                        'username' => 'root',
                        'password' => 'root'
                    );
                    $dbAdapter = new \Zend\Db\Adapter\Adapter($config);
                    $dbTableAuthAdapter = new \Zend\Authentication\Adapter\DbTable($dbAdapter, 'users', 'email', 'password', 'MD5(?)');
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('Tokenizer\Model\TokenizerStorage'));

                    return $authService;
                },
            ),
        );
    }

}
        